#!/usr/bin/python3

# Copyright 2022 Peter Debelak

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see
# <https://www.gnu.org/licenses/>.
import sys
import tkinter as tk
import unicodedata
from itertools import islice
from tkinter import messagebox, ttk


class UnicodeCharacters(tk.Tk):
    app_name = "Unicode Characters"

    def __init__(self):
        super().__init__(className=self.app_name)
        self.option_add("*tearOff", tk.FALSE)
        self["menu"] = self._create_menubar()
        self.title(self.app_name)
        self.character_list = CharacterList(on_copy=self.copy_character)
        self.character_list.grid(row=1, column=0, columnspan=2, sticky=tk.NSEW)

        self.status_bar = Label()
        self.status_bar.grid(row=2, column=0, columnspan=2, sticky=tk.EW)

        self.prev_button = Button(self, text="Previous", command=self.previous_page)
        self.prev_button.grid(row=3, column=0, sticky=tk.W)
        self.next_button = Button(self, text="Next", command=self.next_page)
        self.next_button.grid(row=3, column=1, sticky=tk.E)

        self.search = Entry(
            on_change=self.update_search,
        )
        self.bind("<Alt-c>", self.copy_selected)
        self.search.grid(row=0, column=0, columnspan=2, sticky=tk.EW)
        self.update_pagination()
        self.search.focus_set()

        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)

    def start(self):
        self.mainloop()

    def _create_menubar(self):
        menubar = tk.Menu(self)
        if sys.platform == "darwin":
            meta_char = "Command"
            appmenu = tk.Menu(menubar, name="apple")
            menubar.add_cascade(menu=appmenu, label=self.app_name)
            appmenu.add_command(
                label="About Unicode Characters", command=self.show_about
            )
            appmenu.add_separator()
        else:
            meta_char = "Alt"

        commandmenu = tk.Menu(menubar, name="commands")
        menubar.add_cascade(menu=commandmenu, label="Commands")
        commandmenu.add_command(
            label="Copy Selected Character",
            command=self.copy_selected,
            accelerator="Return",
        )
        self.bind("<Return>", self.copy_selected)

        commandmenu.add_command(
            label="Select Next Character",
            command=self.down_selected,
            accelerator="Control-N",
        )
        self.bind("<Down>", self.down_selected)
        self.bind("<Control-n>", self.down_selected)

        commandmenu.add_command(
            label="Select Previous Character",
            command=self.up_selected,
            accelerator="Control-P",
        )
        self.bind("<Up>", self.up_selected)
        self.bind("<Control-p>", self.up_selected)

        commandmenu.add_command(
            label="Next Page", command=self.next_page, accelerator=f"{meta_char}-N"
        )
        self.bind(f"<{meta_char}-n>", self.next_page)
        commandmenu.add_command(
            label="Previous Page",
            command=self.previous_page,
            accelerator=f"{meta_char}-P",
        )
        self.bind(f"<{meta_char}-p>", self.previous_page)

        if sys.platform == "darwin":
            # macos automatically adds window controls to this menu
            windowmenu = tk.Menu(menubar, name="window")
            menubar.add_cascade(menu=windowmenu, label="Window")

        helpmenu = tk.Menu(menubar, name="help")
        menubar.add_cascade(menu=helpmenu, label="Help")
        helpmenu.add_command(label="About Unicode Characters", command=self.show_about)
        return menubar

    def update_pagination(self):
        if self.character_list.page > 1:
            self.prev_button.enable()
        else:
            self.prev_button.disable()
        if self.character_list.has_next:
            self.next_button.enable()
        else:
            self.next_button.disable()

    def previous_page(self, *args):
        if self.character_list.page <= 1:
            return
        self.character_list.page = self.character_list.page - 1
        self.update_pagination()
        self.search.focus_set()

    def next_page(self, *args):
        if not self.character_list.has_next:
            return
        self.character_list.page = self.character_list.page + 1
        self.update_pagination()
        self.search.focus_set()

    def update_search(self, s):
        self.character_list.search = s
        self.update_pagination()

    def copy_selected(self, *args):
        self.copy_character(self.character_list.selected_character())

    def up_selected(self, *args):
        self.character_list.selected -= 1

    def down_selected(self, *args):
        self.character_list.selected += 1

    def copy_character(self, character):
        self.clipboard_clear()
        self.clipboard_append(character)
        self.status_bar.text = f"{character} copied!"
        self.search.focus_set()

    def show_about(self):
        messagebox.showinfo(
            message="""Searchi for and copy unicode characters (like emoji).

Copyright 2022 Peter Debelak

Licensed under the GNU General Public License v3 or later."""
        )


class CharacterList(tk.Frame):
    def __init__(self, *args, search="", max_items=20, on_copy=None, **kwargs):
        super().__init__(*args, **kwargs)
        self._max_items = max_items
        self._chars = []
        for i in range(max_items):
            char = UnicodeCharacter(self, on_copy=on_copy, on_click=self.on_click)
            char.grid(row=i, column=0, sticky=tk.EW)
            self._chars.append(char)
        self.columnconfigure(0, weight=1)
        self.search = search

    @property
    def selected(self):
        return self._selected

    @selected.setter
    def selected(self, value):
        if value < 0:
            return
        if value >= self._max_items or (
            not self._chars[value].character() and value > 0
        ):
            return
        self._selected = value
        for i, char in enumerate(self._chars):
            char.set_selected(i == value)

    def selected_character(self):
        return self._chars[self.selected].character()

    @property
    def search(self):
        return self._search

    @search.setter
    def search(self, value):
        self._search = value
        self.page = 1

    @property
    def page(self):
        return self._page

    @page.setter
    def page(self, value):
        self._page = value
        self._set_names()

    def on_click(self, char):
        clicked_idx = next(i for i, c in enumerate(self._chars) if char == c)
        self.selected = clicked_idx

    def _set_names(self):
        names = self._lookup_names()
        for _ in range(1, self.page):
            list(islice(names, self._max_items))  # go through previous pages
        for char in self._chars:
            try:
                char.name = next(names)
            except StopIteration:
                char.name = ""
        try:
            next(names)
            self.has_next = True
        except StopIteration:
            self.has_next = False
        self.selected = 0

    def _lookup_names(self):
        for i in range(sys.maxunicode):
            name = unicodedata.name(chr(i), None)
            if name is not None and self._search.lower() in name.lower():
                yield name


class UnicodeCharacter(tk.Frame):
    def __init__(self, *args, on_copy=None, on_click=None, name="", **kwargs):
        super().__init__(*args, highlightthickness=2, **kwargs)
        self._unselected_color = self["highlightbackground"]
        self._on_copy = on_copy
        self._on_click = on_click

        self.bind("<Button>", self.on_click)
        self.name_label = Label(self)
        self.name_label.grid(column=1, row=0, sticky=tk.EW, padx=3)
        self.name_label.bind("<Button>", self.on_click)
        self.character_label = Label(self)
        self.character_label.grid(column=0, row=0, padx=3)
        self.character_label.bind("<Button>", self.on_click)
        self.copy_button = Button(self, text="Copy", command=self.copy_character)
        self.name = name
        self.columnconfigure(1, weight=1)

    def set_selected(self, selected):
        if selected and self.character():
            self["highlightbackground"] = "blue"
        else:
            self["highlightbackground"] = self._unselected_color

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value
        self.name_label.text = value.capitalize()
        self.character_label.text = self.character()
        if value:
            self.copy_button.grid(column=2, row=0, pady=3, padx=3)
        else:
            self.copy_button.grid_forget()

    def character(self):
        if not self.name:
            return ""
        return unicodedata.lookup(self.name)

    def copy_character(self, *args):
        if self._on_copy:
            self._on_copy(self.character())
        self.on_click()

    def on_click(self, *args):
        if self._on_click:
            self._on_click(self)


class Label(ttk.Label):
    def __init__(self, *args, text="", **kwargs):
        super().__init__(*args, **kwargs)
        self._string_var = tk.StringVar()
        self["textvariable"] = self._string_var
        self._string_var.set(text)

    @property
    def text(self):
        return self._string_var.get()

    @text.setter
    def text(self, value):
        return self._string_var.set(value)


class Entry(ttk.Entry):
    def __init__(
        self,
        *args,
        text="",
        on_change=None,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self._change_callback = on_change
        self._string_var = tk.StringVar()
        self._string_var.trace_add("write", self._on_change)
        self["textvariable"] = self._string_var
        self._string_var.set(text)

    def _on_change(self, *args):
        if self._change_callback:
            self._change_callback(self._string_var.get())


class Button(ttk.Button):
    def enable(self):
        self.state(["!disabled"])

    def disable(self):
        self.state(["disabled"])


if __name__ == "__main__":
    UnicodeCharacters().start()
