# Unicode Characters

An app for searching for and copying unicode characters (like emoji).

## Installation

The [install.sh](./install.sh) script in this directory can be used to
install this on Linux or macOS to the user application directory
(although getting the icon working on mac requires some more fiddling
around).

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

## Artwork

This includes a copy of the input-keyboard png image from the [GNOME
Project](http://www.gnome.org) via the [CC-BY-SA
license](http://creativecommons.org/licenses/by-sa/3.0/).
