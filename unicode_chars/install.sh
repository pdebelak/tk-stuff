#!/bin/sh

script_dir="$( cd "$( dirname "$0" )" && pwd )"

os="$(uname -s)"

mkdir -p "$HOME/.local/bin"
local_app_dir="$HOME/.local/share/unicode_characters"
mkdir -p "$local_app_dir"
sed "s|#!/usr/bin/python3|#!$(command -v python3)|" "$script_dir/unicode_chars.py" > "$script_dir/unicode_chars"
install -m755 "$script_dir/unicode_chars" "$local_app_dir"
rm -f "$script_dir/unicode_chars"
install "$script_dir/input-keyboard.png" "$local_app_dir"

if [ "$os" = "Linux" ]; then
  mkdir -p "$HOME/.local/share/applications"
  echo "[Desktop Entry]
Encoding=UTF-8
Version=1.0
Type=Application
Terminal=false
Exec=$local_app_dir/unicode_chars
Name=Unicode Characters
Icon=$local_app_dir/input-keyboard.png
" > "$HOME/.local/share/applications/unicode_chars.desktop"
elif [ "$os" = "Darwin" ]; then
  app_dir="$HOME/Applications/Unicode Characters.app/Contents/MacOS"
  mkdir -p "$app_dir"
  app_script="$app_dir/Unicode Characters"
  echo "#!/bin/sh
$local_app_dir/unicode_chars" >"$app_script"
  chmod +x "$app_script"
  echo '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
  <key>CFBundleExecutable</key>
  <string>Unicode Characters</string>
</dict>
</plist>' >"$app_dir/../Info.plist"
else
  echo "Unhandled os: $os" >&2
  exit 1
fi
