#!/bin/sh

script_dir="$( cd "$( dirname "$0" )" && pwd )"

os="$(uname -s)"

mkdir -p "$HOME/.local/bin"
local_app_dir="$HOME/.local/share/kui"
mkdir -p "$local_app_dir"
sed \
  -e "s|#!/usr/bin/python3|#!$(command -v python3)|" \
  -e 's|KUBECTL = "/usr/local/bin/kubectl"|KUBECTL = "'"$(command -v kubectl)"'"|' \
  "$script_dir/kui.py" > "$script_dir/kui"
install -m755 "$script_dir/kui" "$local_app_dir"
rm -f "$script_dir/kui"

if [ "$os" = "Linux" ]; then
  mkdir -p "$HOME/.local/share/applications"
  echo "[Desktop Entry]
Encoding=UTF-8
Version=1.0
Type=Application
Terminal=false
Exec=$local_app_dir/kui
Name=kui
" > "$HOME/.local/share/applications/kui.desktop"
elif [ "$os" = "Darwin" ]; then
  app_dir="$HOME/Applications/kui.app/Contents/MacOS"
  mkdir -p "$app_dir"
  app_script="$app_dir/kui"
  echo "#!/bin/sh
$local_app_dir/kui" >"$app_script"
  chmod +x "$app_script"
  echo '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
  <key>CFBundleExecutable</key>
  <string>kui</string>
</dict>
</plist>' >"$app_dir/../Info.plist"
else
  echo "Unhandled os: $os" >&2
  exit 1
fi
