#!/usr/bin/python3

# Copyright 2022 Peter Debelak

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# “Software”), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import shlex
import subprocess
import sys
import tkinter as tk
import tkinter.simpledialog
from threading import Thread
from tkinter import ttk
from uuid import uuid4

KUBECTL = "/usr/local/bin/kubectl"


class KUI(tk.Tk):
    app_name = "kui"

    def __init__(self):
        super().__init__(className=self.app_name)
        self.option_add("*tearOff", tk.FALSE)
        self.kubectl = Kubectl()

        self.context_chooser = ttk.Combobox(self)
        self.context_chooser.state(["readonly"])
        self.context_chooser.bind("<<ComboboxSelected>>", self.set_context)
        self.context_chooser.grid(row=0, column=1, sticky=tk.W)
        context_label = ttk.Label(self, text="Context:")
        context_label.grid(row=0, column=0, sticky=tk.E)

        self.namespace_chooser = ttk.Combobox(self)
        self.namespace_chooser.state(["readonly"])
        self.namespace_chooser.bind("<<ComboboxSelected>>", self.set_namespace)
        self.namespace_chooser.grid(row=1, column=1, sticky=tk.W)
        namespace_label = ttk.Label(self, text="Namespace:")
        namespace_label.grid(row=1, column=0, sticky=tk.E)

        hr1 = ttk.Separator(self, orient=tk.HORIZONTAL)
        hr1.grid(row=2, column=0, columnspan=2, sticky=tk.EW)

        self.pod_list = PodList(
            self, kubectl=self.kubectl, refresh_pods=self.refresh_pods
        )
        self.pod_list.grid(row=3, column=0, columnspan=2, sticky=tk.NSEW)

        hr2 = ttk.Separator(self, orient=tk.HORIZONTAL)
        hr2.grid(row=4, column=0, columnspan=2, sticky=tk.EW)

        self.service_list = ServiceList(self, kubectl=self.kubectl)
        self.service_list.grid(row=5, column=0, columnspan=2, sticky=tk.S)

        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)
        self.rowconfigure(3, weight=1)

        run_async(self, self.kubectl.get_contexts, self.got_contexts)

    def got_contexts(self, _result):
        self.context_chooser["values"] = self.kubectl.contexts
        self.context_chooser.set(self.kubectl.context)
        run_async(self, self.kubectl.get_namespaces, self.got_namespaces)

    def set_context(self, *args):
        self.kubectl.context = self.context_chooser.get()
        run_async(self, self.kubectl.get_namespaces, self.got_namespaces)

    def refresh_pods(self):
        run_async(self, self.kubectl.get_pods, self.got_pods)
        run_async(self, self.kubectl.get_services, self.got_services)

    def got_namespaces(self, _result):
        self.namespace_chooser["values"] = self.kubectl.namespaces
        self.namespace_chooser.set(self.kubectl.namespace)
        self.refresh_pods()

    def set_namespace(self, *args):
        self.kubectl.namespace = self.namespace_chooser.get()
        run_async(self, self.kubectl.get_pods, self.got_pods)
        run_async(self, self.kubectl.get_services, self.got_services)

    def got_pods(self, _result):
        self.pod_list.pods = self.kubectl.pods

    def got_services(self, _result):
        self.service_list.services = self.kubectl.services

    def start(self):
        self.mainloop()


class PodList(tk.Frame):
    def __init__(self, *args, kubectl, refresh_pods, **kwargs):
        super().__init__(*args, **kwargs)
        self.kubectl = kubectl
        self.refresh_pods = refresh_pods
        self.pods = []

        refresh_button = ttk.Button(self, text="↻", command=self.refresh_pods)
        refresh_button.grid(row=0, column=4, sticky=tk.E)
        name_header = ttk.Label(self, text="Pod")
        name_header.grid(row=1, column=0, sticky=tk.W)
        status_header = ttk.Label(self, text="Status")
        status_header.grid(row=1, column=1, sticky=tk.W)
        container_header = ttk.Label(self, text="Containers")
        container_header.grid(row=1, column=2, sticky=tk.W)

        self.columnconfigure(0, weight=2)
        self.columnconfigure(1, weight=1)

    @property
    def pods(self):
        try:
            return self._pods
        except AttributeError:
            return []

    @pods.setter
    def pods(self, pods):
        for pod in self.pods:
            pod.grid_forget()
        new_pods = []
        for i, p in enumerate(pods):
            pod = Pod(self, kubectl=self.kubectl, pod_info=p)
            self.rowconfigure(i, weight=2)
            pod.grid(row=i + 2)
            new_pods.append(pod)
        self._pods = new_pods


class Pod:
    def __init__(self, parent, kubectl, pod_info):
        self.kubectl = kubectl
        self.pod_name = pod_info.name
        self.name_label = ttk.Label(parent, text=self.pod_name)
        self.status_label = ttk.Label(parent, text=pod_info.status_summary())
        self.container = None
        self.container_chooser = ttk.Combobox(parent)
        self.container_chooser.state(["readonly"])
        self.container_chooser.bind("<<ComboboxSelected>>", self.set_container)
        self.logs_button = ttk.Button(parent, text="Logs", command=self.show_logs)
        self.shell_button = ttk.Button(parent, text="Shell", command=self.start_shell)
        run_async(
            parent,
            lambda: self.kubectl.containers_for(pod_info.name),
            self.got_containers,
        )

    def grid(self, row):
        self.name_label.grid(row=row, column=0, sticky=tk.EW, padx=(0, 6))
        self.status_label.grid(row=row, column=1, sticky=tk.EW, padx=(0, 6))
        self.container_chooser.grid(row=row, column=2, sticky=tk.E)
        self.logs_button.grid(row=row, column=3, sticky=tk.E)
        self.shell_button.grid(row=row, column=4, sticky=tk.E)

    def grid_forget(self):
        self.name_label.grid_forget()
        self.status_label.grid_forget()
        self.container_chooser.grid_forget()
        self.logs_button.grid_forget()
        self.shell_button.grid_forget()

    def show_logs(self):
        self.kubectl.logs(self.pod_name, self.container)

    def start_shell(self):
        command = tkinter.simpledialog.askstring(
            "Command", "Command", initialvalue="bash"
        )
        print(command)
        if command:
            self.kubectl.shell(self.pod_name, self.container, command)

    def set_container(self, *args):
        self.container = self.container_chooser.get()

    def got_containers(self, containers):
        self.container_chooser["values"] = containers
        self.container = containers[0]
        self.container_chooser.set(self.container)


class ServiceList(tk.Frame):
    def __init__(self, *args, kubectl, **kwargs):
        super().__init__(*args, **kwargs)
        self.kubectl = kubectl
        self.services = []

        name_header = ttk.Label(self, text="Service")
        name_header.grid(row=0, column=0, sticky=tk.W)
        status_header = ttk.Label(self, text="Port")
        status_header.grid(row=0, column=1, sticky=tk.W)

    @property
    def services(self):
        try:
            return self._services
        except AttributeError:
            return []

    @services.setter
    def services(self, services):
        for service in self.services:
            service.grid_forget()
        new_services = []
        for i, s in enumerate(services):
            service = Service(self, kubectl=self.kubectl, service_info=s)
            self.rowconfigure(i, weight=2)
            service.grid(row=i + 1)
            new_services.append(service)
        self._services = new_services


class Service:
    def __init__(self, parent, kubectl, service_info):
        self.kubectl = kubectl
        self.service_name = service_info[0]
        self.port = str(service_info[1][0])
        self.name_label = ttk.Label(parent, text=self.service_name)
        self.port_label = ttk.Label(parent, text=self.port)
        self.port_forward_button = ttk.Button(
            parent, text="Port Forward", command=self.port_forward
        )

    def grid(self, row):
        self.name_label.grid(row=row, column=0, sticky=tk.W, padx=(0, 6))
        self.port_label.grid(row=row, column=1, sticky=tk.W, padx=(0, 6))
        self.port_forward_button.grid(row=row, column=2, sticky=tk.W)

    def grid_forget(self):
        self.name_label.grid_forget()
        self.port_label.grid_forget()
        self.port_forward_button.grid_forget()

    def port_forward(self):
        self.kubectl.port_forward(self.service_name, self.port)


class Kubectl:
    def __init__(self):
        self.contexts = []
        self.namespaces = []
        self.context = None
        self.namespace = None
        self.pods = []
        self.services = []

    def get_contexts(self):
        result = subprocess.run(
            [KUBECTL, "config", "get-contexts", "--no-headers"],
            capture_output=True,
            check=True,
        )
        contexts = []
        for line in result.stdout.decode("utf-8").split("\n"):
            if not line.strip():
                continue
            name = line[1:].strip().split()[0]
            if line.startswith("*"):
                self.context = name
            contexts.append(name)
        self.contexts = contexts
        if not self.context:
            self.context = self.contexts[0]

    def get_namespaces(self):
        result = subprocess.run(
            [KUBECTL, "get", "namespaces", "--no-headers", "--context", self.context],
            capture_output=True,
            check=True,
        )
        namespaces = []
        for line in result.stdout.decode("utf-8").split("\n"):
            if not line.strip():
                continue
            name = line.strip().split()[0]
            namespaces.append(name)
        self.namespaces = namespaces
        if "default" in self.namespaces:
            self.namespace = "default"
        else:
            self.namespace = self.namespaces[0]

    def get_pods(self):
        result = subprocess.run(
            [
                KUBECTL,
                "get",
                "pods",
                "--no-headers",
                "--context",
                self.context,
                "--namespace",
                self.namespace,
            ],
            capture_output=True,
            check=True,
        )
        pods = []
        for line in result.stdout.decode("utf-8").split("\n"):
            if not line.strip():
                continue
            fields = line.strip().split()
            pods.append(PodInfo(*fields))
        self.pods = pods

    def get_services(self):
        result = subprocess.run(
            [
                KUBECTL,
                "get",
                "services",
                "--no-headers",
                "--context",
                self.context,
                "--namespace",
                self.namespace,
            ],
            capture_output=True,
            check=True,
        )
        services = []
        for line in result.stdout.decode("utf-8").split("\n"):
            if not line.strip():
                continue
            fields = line.strip().split()
            ports = [p.split("/")[0] for p in fields[4].split(",")]
            services.append((fields[0], ports))
        self.services = services

    def containers_for(self, pod_name):
        result = subprocess.run(
            [
                KUBECTL,
                "get",
                "pods",
                pod_name,
                "--context",
                self.context,
                "--namespace",
                self.namespace,
                "-o",
                "jsonpath='{.spec.containers[*].name}'",
            ],
            capture_output=True,
            check=True,
        )
        return result.stdout.decode("utf-8")[1:-1].split(" ")

    def logs(self, pod_name, container):
        args = [
            KUBECTL,
            "logs",
            "--context",
            self.context,
            "--namespace",
            self.namespace,
            "-f",
            pod_name,
        ]
        if container is not None:
            args.extend(["-c", container])
        self._run_in_terminal(*args)

    def shell(self, pod_name, container, command):
        args = [
            KUBECTL,
            "exec",
            "-it",
            "--context",
            self.context,
            "--namespace",
            self.namespace,
            pod_name,
        ]
        if container is not None:
            args.extend(["-c", container])
        args.extend(["--", *shlex.split(command)])
        self._run_in_terminal(*args)

    def port_forward(self, service, port):
        self._run_in_terminal(
            KUBECTL,
            "port-forward",
            f"svc/{service}",
            f"{port}:{port}",
            "--context",
            self.context,
            "--namespace",
            self.namespace,
        )

    def _run_in_terminal(self, *cmd):
        if sys.platform == "darwin":
            # macos, use default terminal app
            subprocess.Popen(
                [
                    "osascript",
                    "-e",
                    f'tell application "Terminal" to do script "{shlex.join(cmd)}"',
                ]
            )
        else:
            # we assume linux and gnome, cause that's what I use
            subprocess.Popen(["gnome-terminal", "--", *cmd])


class PodInfo:
    def __init__(self, name, ready, status, *restarts_and_age):
        self.name = name
        self.ready = ready
        self.status = status
        self.restarts = " ".join(restarts_and_age[:-1])
        self.age = restarts_and_age[-1]
        self.containers = []

    def status_summary(self) -> str:
        return f"Status: {self.status} Ready: {self.ready} Restarts: {self.restarts} Age: {self.age}"


def run_async(root, func, callback):
    event_name = f"<<asyncevent{uuid4()}>>"
    result = None

    def event_handler(*args):
        callback(result)
        root.unbind(event_name)

    root.bind(event_name, event_handler)

    def target():
        nonlocal result
        result = func()
        root.event_generate(event_name)

    t = Thread(target=target)
    t.start()


if __name__ == "__main__":
    KUI().start()
