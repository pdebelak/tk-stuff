kui
===

A simple kubectl gui.

## Installation

The [install.sh](./install.sh) script in this directory can be used to
install this on Linux or macOS to the user application directory.

## License

This program is licensed under the MIT license. See LICENSE file for
details.
