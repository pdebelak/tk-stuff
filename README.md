# tk-stuff

Some small tk apps built in python

## [Unicode Characters](./unicode_chars)

An app for searching for and copying unicode characters (like emoji).

## [kui](./kui)

A simple kubectl gui. Shows pod statuses and allows for tailing logs,
execing to containers, and forwarding ports.
